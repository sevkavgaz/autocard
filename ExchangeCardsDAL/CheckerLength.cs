﻿// Created : 2014 02 27 10:52 
// Changed : 2014 03 03 8:51 : Караваев Вадим

using System.Collections.Generic;


namespace ExchangeCardsDAL
{
	public partial class Department
	{
		private static FieldCheckLength _checkLengthName =
			new FieldCheckLength(3, 200, "Название отдела");
		private static FieldCheckLength _checkLengthPhone =
			new FieldCheckLength(3, 20, "Номер телефона отдела");
		private static FieldCheckLength _checkLengthIdentifier =
			new FieldCheckLength(3, 10, "Идентификатор карт");

		public static FieldCheckLength CheckLengthName { get { return _checkLengthName; } }
		public static FieldCheckLength CheckLengthPhone { get { return _checkLengthPhone; } }
		public static FieldCheckLength CheckLengthIdentifier { get { return _checkLengthIdentifier; } }


		/// <summary>
		///     Возвращает результат проверки всех полей на длину
		/// </summary>
		public static bool CheckLength(string name, string phone, string identifier, ref string msg)
		{
			var checkDictionary =
				new Dictionary<FieldCheckLength, string>
				{
					{_checkLengthName, name},
					{_checkLengthPhone, phone},
					{_checkLengthIdentifier, identifier}
				};
			return new CheckerLength(checkDictionary).Check(ref msg);
		}
	}


	public partial class Employee
	{
		private static FieldCheckLength _checkLengthLastName =
			new FieldCheckLength(2, 50, "Фамилия сотрудника");
		private static FieldCheckLength _checkLengthFirstName =
			new FieldCheckLength(2, 50, "Имя сотрудника");
		private static FieldCheckLength _checkLengthMiddleName =
			new FieldCheckLength(0, 50, "Отчество сотрудника");
		private static FieldCheckLength _checkLengthAddress =
			new FieldCheckLength(5, 400, "Адрес сотрудника");
		private static FieldCheckLength _checkLengthPhone =
			new FieldCheckLength(3, 20, "Телефон сотрудника");
		private static FieldCheckLength _checkLengthPost =
			new FieldCheckLength(5, 250, "Должность сотрудника");
		private static FieldCheckLength _checkLengthPostMsg =
			new FieldCheckLength(5, 500, "Обращение к сотруднику");

		public static FieldCheckLength CheckLengthLastName { get { return _checkLengthLastName; } }
		public static FieldCheckLength CheckLengthFirstName { get { return _checkLengthFirstName; } }
		public static FieldCheckLength CheckLengthMiddleName { get { return _checkLengthMiddleName; } }
		public static FieldCheckLength CheckLengthAddress { get { return _checkLengthAddress; } }
		public static FieldCheckLength CheckLengthPhone { get { return _checkLengthPhone; } }
		public static FieldCheckLength CheckLengthPost { get { return _checkLengthPost; } }
		public static FieldCheckLength CheckLengthPostMsg { get { return _checkLengthPostMsg; } }

		/// <summary>
		///     Возвращает результат проверки всех полей на длину
		/// </summary>
		public static bool CheckLength(string lastName, string firstName, string middleName,
			string address, string phone, string post, string postMsg, ref string msg)
		{
			var checkDictionary =
				new Dictionary<FieldCheckLength, string>
				{
					{_checkLengthLastName, lastName},
					{_checkLengthFirstName, firstName},
					{_checkLengthMiddleName, middleName},
					{_checkLengthAddress, address},
					{_checkLengthPhone, phone},
					{_checkLengthPost, post},
					{_checkLengthPostMsg, postMsg},
				};
			return new CheckerLength(checkDictionary).Check(ref msg);
		}
	}


	public partial class Card
	{
		private static FieldCheckLength _checkLengthTheme =
			new FieldCheckLength(3, 50, "Тема карты");
		private static FieldCheckLength _checkLengthText =
			new FieldCheckLength(3, 2000, "Текст карты");

		public static FieldCheckLength CheckLengthTheme { get { return _checkLengthTheme; } }
		public static FieldCheckLength CheckLengthText { get { return _checkLengthText; } }

		/// <summary>
		///     Возвращает результат проверки всех полей на длину
		/// </summary>
		public static bool CheckLength(string theme, string text, ref string msg)
		{
			var checkDictionary =
				new Dictionary<FieldCheckLength, string>
				{
					{_checkLengthTheme, theme},
					{_checkLengthText, text},
				};
			return new CheckerLength(checkDictionary).Check(ref msg);
		}
	}


	/// <summary>
	///     Структура содержащая информацию о поле проверяемом на длину
	/// </summary>
	public struct FieldCheckLength
	{
		private int _min;
		private int _max;
		private string _fieldName;

		public int Min { get { return _min; } }
		public int Max { get { return _max; } }
		public string FieldName { get { return _fieldName; } }

		public FieldCheckLength(int min, int max, string fieldName)
		{
			_min = min;
			_max = max;
			_fieldName = fieldName;
		}
	}


	/// <summary>
	///     Объект содержащий результаты проверки полей на длину
	/// </summary>
	internal class CheckerLength
	{
		private Dictionary<FieldCheckLength, string> _chekingDictionary;

		internal CheckerLength(Dictionary<FieldCheckLength, string> chDictionary)
		{
			_chekingDictionary = chDictionary;
		}

		/// <summary>
		///     Возвращает результат проверки полей на длину
		/// </summary>
		internal bool Check(ref string msg)
		{
			foreach (var ch in _chekingDictionary)
			{
				msg = "Поле \"" + ch.Key.FieldName
					+ "\" должно содержать символов от " + ch.Key.Min + " до " + ch.Key.Max;
				// если значение слишком длинное
				if (ch.Key.Max<ch.Value.Length)
				{
					msg += ".\nВведите другое значение, покороче.";
					return false;
				}
				// если значение слишком короткое
				if (ch.Key.Min>ch.Value.Length)
				{
					msg += ".\nВведите другое, более длинное значение.";
					return false;
				}
			}
			return true;
		}
	}
}