﻿// Created : 2014 03 05 9:39 
// Changed : 2014 03 17 11:52 : Караваев Вадим

using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Windows;
using ExchangeCardsDAL;


namespace AutoCARD
{
	/// <summary>
	///     Окно c настройка фильтров
	/// </summary>
	public partial class Filters
	{
		//ссылка на главное окно
		private MainWindow _mainWindow;

		/// <summary>
		///     Конструктор окна c фильтрами
		/// </summary>
		/// <param name="mainWindow">ссылка на экземпляр главного окна</param>
		public Filters(MainWindow mainWindow)
		{
			_mainWindow = mainWindow;
			InitializeComponent();
			// наверное так делать нельзя ну а вообще вот таким извращенским способом
			// я задаю начальные, дефолтные значения настройкам фильтров
			//.. для карточек..
			{
				// статус
				comboBoxStatus.ItemsSource = new List<string>
				{
					"Все",
					"Подтверждено",
					"Не подтверждено"
				};
				// тип карты
				comboBoxType.ItemsSource = new List<string>
				{
					"Все",
					"Служебная записка",
					"Докладная записка"
				};
				resetFiltersCards_Click(null, null);
			}
			// .. для сотрудников..
			{
				// критeрий "начальник?"
				comboBoxIsBoss.ItemsSource = new List<string>
				{
					"Все",
					"Является начальником",
					"Не является начальником"
				};
				resetFiltersEmployees_Click(null, null);
			}
			// .. и для отделов
			{
				// есть ли начальник у отдела?
				comboBoxBoss.ItemsSource = new List<string>
				{
					"Все",
					"Начальник не назначен",
					"Начальник назначен"
				};
				resetFiltersDepartments_Click(null, null);
			}
		}

		/// <summary>
		///     Сброс фильтров для карточек
		/// </summary>
		private void resetFiltersCards_Click(object sender, RoutedEventArgs r)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				// сброс даты
				dateStart.SelectedDate = new DateTime(1970, 1, 1);
				dateEnd.SelectedDate = DateTime.Now.AddYears(10);
				// сброс статуса
				comboBoxStatus.SelectedIndex = 0;
				// сброс типа карты
				comboBoxType.SelectedIndex = 0;

				// сброс списка отделов и сотрудников
				var allDepartments = from e in ece.Departments
					where e.Employees.Any()
					select e.Name;
				var departmentsList = allDepartments.ToList();
				departmentsList.InsertRange(0, new[] {"Все"});

				comboBoxDepartmentAuthor.ItemsSource =
					comboBoxDepartmentCompiler.ItemsSource =
						comboBoxDepartmentDestination.ItemsSource = departmentsList;

				comboBoxDepartmentAuthor.SelectedIndex =
					comboBoxDepartmentDestination.SelectedIndex =
						comboBoxDepartmentCompiler.SelectedIndex = 0;

				comboBoxAuthor.ItemsSource =
					comboBoxCompiler.ItemsSource =
						comboBoxDestination.ItemsSource = null;
			}
			// обновить список карточек только при нажатии на кнопку
			if (sender!=null)
			{
				_mainWindow.UpdateGridCards(null, null);
			}
		}

		/// <summary>
		///     Сброс фильтров для сотрудников
		/// </summary>
		private void resetFiltersEmployees_Click(object sender, RoutedEventArgs r)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				//сброс фильтра по отделам
				var departmentsList = (from e in ece.Departments
					where e.Employees.Any()
					select e.Name).ToList();
				departmentsList.InsertRange(0, new[] {"Все"});
				comboBoxDepartment.ItemsSource = departmentsList;
				comboBoxDepartment.SelectedIndex = 0;
				// сброс критерия "начальник?"
				comboBoxIsBoss.SelectedIndex = 0;
				// сброс фильтра по этажам
				// все все все кабинеты
				var allRoom = from e in ece.Employees
					select SqlFunctions.StringConvert((double) e.Room).Trim();
				var allRoomList = allRoom.ToList();
				// все все этажи
				var allFloorList = new List<string>();
				allRoomList.ForEach(x => allFloorList.Add(x.Substring(0, 1)));
				// все все этажи без дубликатов, и отсортированные
				var allFloorListNoDuplicates = allFloorList.Distinct().ToList();
				allFloorListNoDuplicates.Sort();
				allFloorListNoDuplicates.InsertRange(0, new[] {"Все"});
				comboBoxFloor.ItemsSource = allFloorListNoDuplicates;
				comboBoxFloor.SelectedIndex = 0;
			}
			// обновить список сотрудников только при нажатии на кнопку
			if (sender!=null)
			{
				_mainWindow.UpdateGridEmployees(null, null);
			}
		}

		/// <summary>
		///     Сброс фильтров для отделов
		/// </summary>
		private void resetFiltersDepartments_Click(object sender, RoutedEventArgs r)
		{
			// сброс фильтров для критерия "есть ли начальник"
			comboBoxBoss.SelectedIndex = 0;
			// сброс количества сотрудников
			employeesStart.Value = 0;
			employeesEnd.Value = 9999;
			// обновить список отделов только при нажатии на кнопку
			if (sender!=null)
			{
				_mainWindow.UpdateGridDepartments(null, null);
			}
		}

		/// <summary>
		///     обновление списка сотрудников в соответсвии с выбранным отделом
		/// </summary>
		private void UpdateComboBoxEmployees(object sender, EventArgs x)
		{
			// заполняем выпадающий список "Автор" "Составитель" "Получатель"
			using (var ece = new ExchangeCardsEntities())
			{
				// узнаем id выбранных отделов
				var convertDepartmentAuthorToId = (from e in ece.Departments
					where e.Name==comboBoxDepartmentAuthor.Text
					select e.Id).FirstOrDefault();
				var convertDepartmentCompilerToId = (from e in ece.Departments
					where e.Name==comboBoxDepartmentCompiler.Text
					select e.Id).FirstOrDefault();
				var convertDepartmentDestinationToId = (from e in ece.Departments
					where e.Name==comboBoxDepartmentDestination.Text
					select e.Id).FirstOrDefault();

				// заполняем поля сотрудниками
				var employeesForDepartmentAuthor = (from e in ece.Employees
					where e.Department==convertDepartmentAuthorToId
					select e.LastName + " " + e.FirstName + " "
						+ e.MiddleName + " [" + e.Post + "]").ToList();
				var employeesForDepartmentCompiler = (from e in ece.Employees
					where e.Department==convertDepartmentCompilerToId
					select e.LastName + " " + e.FirstName + " "
						+ e.MiddleName + " [" + e.Post + "]").ToList();
				var employeesForDepartmentDestination = (from e in ece.Employees
					where e.Department==convertDepartmentDestinationToId
					select e.LastName + " " + e.FirstName + " "
						+ e.MiddleName + " [" + e.Post + "]").ToList();

				employeesForDepartmentAuthor.InsertRange(0, new[] {"Все"});
				employeesForDepartmentCompiler.InsertRange(0, new[] {"Все"});
				employeesForDepartmentDestination.InsertRange(0, new[] {"Все"});

				// заполняем выпадающие списки
				comboBoxAuthor.ItemsSource = employeesForDepartmentAuthor;
				comboBoxCompiler.ItemsSource = employeesForDepartmentCompiler;
				comboBoxDestination.ItemsSource = employeesForDepartmentDestination;
				comboBoxAuthor.SelectedIndex = 0;
				comboBoxCompiler.SelectedIndex = 0;
				comboBoxDestination.SelectedIndex = 0;
			}
		}

		/// <summary>
		///     Нажатие на кнопку "Применить"
		/// </summary>
		private void apply_Click(object sender, RoutedEventArgs x)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				// запросы для определения id отдела по его названию
				// COPY_PASTE ВЫШЕ УЖЕ ЕСТЬ, ПЕРЕДЕЛАТЬ В МЕТОД
				var convertDepartmentAuthorToId = (from e in ece.Departments
					where e.Name==comboBoxDepartmentAuthor.Text
					select e.Id).FirstOrDefault();
				var convertDepartmentCompilerToId = (from e in ece.Departments
					where e.Name==comboBoxDepartmentCompiler.Text
					select e.Id).FirstOrDefault();
				var convertDepartmentDestinationToId = (from e in ece.Departments
					where e.Name==comboBoxDepartmentDestination.Text
					select e.Id).FirstOrDefault();
				// узнаем Id Автора, Составителя, получателя по значению выпадающего списка
				// Ненадежный способ! Переделать!
				var convertAuthorToId = (from e in ece.Employees
					where e.LastName + " " + e.FirstName + " "
						+ e.MiddleName + " [" + e.Post + "]"==comboBoxAuthor.Text
					select e.Id).FirstOrDefault();
				var convertCompilerToId = (from e in ece.Employees
					where e.LastName + " " + e.FirstName + " "
						+ e.MiddleName + " [" + e.Post + "]"==comboBoxCompiler.Text
					select e.Id).FirstOrDefault();
				var convertDestinationToId = (from e in ece.Employees
					where e.LastName + " " + e.FirstName + " "
						+ e.MiddleName + " [" + e.Post + "]"==comboBoxDestination.Text
					select e.Id).FirstOrDefault();
				// карта фильтров для карточек
				{
					var mapFilterCards = new Dictionary<string, object>
					{
						{"dateStart", dateStart.DisplayDate},
						{"dateEnd", dateEnd.DisplayDate}
					};
					if (comboBoxStatus.SelectedIndex>0)
					{
						mapFilterCards.Add("status", comboBoxStatus.SelectedIndex==1);
					}
					if (comboBoxType.SelectedIndex>0)
					{
						mapFilterCards.Add("type", comboBoxType.Text);
					}
					if (comboBoxDepartmentAuthor.SelectedIndex>0)
					{
						mapFilterCards.Add("departmentAuthor", convertDepartmentAuthorToId);
						if (comboBoxAuthor.SelectedIndex>0)
						{
							mapFilterCards.Add("author", convertAuthorToId);
						}
					}
					if (comboBoxDepartmentCompiler.SelectedIndex>0)
					{
						mapFilterCards.Add("departmentCompiler", convertDepartmentCompilerToId);
						if (comboBoxCompiler.SelectedIndex>0)
						{
							mapFilterCards.Add("compiler", convertCompilerToId);
						}
					}
					if (comboBoxDepartmentDestination.SelectedIndex>0)
					{
						mapFilterCards.Add("departmentDestination", convertDepartmentDestinationToId);
						if (comboBoxDestination.SelectedIndex>0)
						{
							mapFilterCards.Add("destination", convertDestinationToId);
						}
					}
					_mainWindow.dataGridCards.ItemsSource = GetFilterCards(mapFilterCards);
				}
				// карта фильтров для сотрудников
				{
					var mapFilterEmployees = new Dictionary<string, object>();
					if (comboBoxDepartment.SelectedIndex>0)
					{
						mapFilterEmployees.Add("department", comboBoxDepartment.Text);
					}
					if (comboBoxIsBoss.SelectedIndex>0)
					{
						mapFilterEmployees.Add("isBoss", comboBoxIsBoss.SelectedIndex==1);
					}
					if (comboBoxFloor.SelectedIndex>0)
					{
						mapFilterEmployees.Add("floor", comboBoxFloor.Text);
					}
					_mainWindow.dataGridEmployees.ItemsSource = GetFilterEmployees(mapFilterEmployees);
				}

				// карта фильтров для отделов
				{
					var mapFilterDepartments = new Dictionary<string, object>
					{
						{"employeesStart", Convert.ToInt32(employeesStart.Value)},
						{"employeesEnd", Convert.ToInt32(employeesEnd.Value)}
					};
					if (comboBoxBoss.SelectedIndex>0)
					{
						mapFilterDepartments.Add("boss", comboBoxBoss.SelectedIndex==1);
					}
					_mainWindow.dataGridDepartments.ItemsSource =
						GetFilterDepartments(mapFilterDepartments);
				}
			}
		}

		/// <summary>
		///     Возвращает отфильтрованный список карточек
		/// </summary>
		/// <param name="mapFilter">Карта фильтров</param>
		private static IEnumerable<Card> GetFilterCards(Dictionary<string, object> mapFilter)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				ece.ContextOptions.LazyLoadingEnabled = false;
				var filterAll = from e in ece.Cards
					select e.Id;

				// фильтрация "дата от"
				if (mapFilter.ContainsKey("dateStart"))
				{
					var dateStart = (DateTime) mapFilter["dateStart"];
					filterAll = filterAll.Intersect(from e in ece.Cards
						where e.Date>dateStart
						select e.Id);
				}
				// фильтрация "дата до"
				if (mapFilter.ContainsKey("dateEnd"))
				{
					var dateEnd = (DateTime) mapFilter["dateEnd"];
					filterAll = filterAll.Intersect(from e in ece.Cards
						where e.Date<dateEnd
						select e.Id);
				}
				// фильтрация "статус"
				if (mapFilter.ContainsKey("status"))
				{
					var status = (bool) mapFilter["status"];
					filterAll = filterAll.Intersect(from e in ece.Cards
						where e.Confirmed==status
						select e.Id);
				}
				// фильтрация "тип"
				if (mapFilter.ContainsKey("type"))
				{
					var type = (string) mapFilter["type"];
					filterAll = filterAll.Intersect(from e in ece.Cards
						where e.Type==type
						select e.Id);
				}
				// фильтрация "автор"
				if (mapFilter.ContainsKey("departmentAuthor"))
				{
					var department = (int) mapFilter["departmentAuthor"];
					var departmentAuthors = from e in ece.Employees
						where e.Department==department
						select e.Id;
					filterAll = filterAll.Intersect(from e in ece.Cards
						where departmentAuthors.Contains(e.Author)
						select e.Id);
					if (mapFilter.ContainsKey("author"))
					{
						var author = (int) mapFilter["author"];
						filterAll = filterAll.Intersect(from e in ece.Cards
							where e.Author==author
							select e.Id);
					}
				}
				// фильтрация "составитель"
				if (mapFilter.ContainsKey("departmentCompiler"))
				{
					var department = (int) mapFilter["departmentCompiler"];
					var departmentCompilers = from e in ece.Employees
						where e.Department==department
						select e.Id;
					filterAll = filterAll.Intersect(from e in ece.Cards
						where departmentCompilers.Contains(e.Compiler)
						select e.Id);
					if (mapFilter.ContainsKey("compiler"))
					{
						var compiler = (int) mapFilter["compiler"];
						filterAll = filterAll.Intersect(from e in ece.Cards
							where e.Compiler==compiler
							select e.Id);
					}
				}
				// фильтрация "получатель"
				if (mapFilter.ContainsKey("departmentDestination"))
				{
					var department = (int) mapFilter["departmentDestination"];
					var departmentDestination = from e in ece.Employees
						where e.Department==department
						select e.Id;
					filterAll = filterAll.Intersect(from e in ece.Cards
						where departmentDestination.Contains(e.Destination)
						select e.Id);
					if (mapFilter.ContainsKey("destination"))
					{
						var destination = (int) mapFilter["destination"];
						filterAll = filterAll.Intersect(from e in ece.Cards
							where e.Destination==destination
							select e.Id);
					}
				}

				// отфильтрованный список сотрудников
				return (from e in ece.Cards
					where filterAll.Contains(e.Id)
					select e).ToList();
			}
		}

		/// <summary>
		///     Возвращает отфильтрованный список сотрудников
		/// </summary>
		/// <param name="mapFilter">Карта фильтров</param>
		private static IEnumerable<Employee> GetFilterEmployees(Dictionary<string, object> mapFilter)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				ece.ContextOptions.LazyLoadingEnabled = false;
				var filterAll = from e in ece.Employees
					select e.Id;

				// фильтрация по отделу
				if (mapFilter.ContainsKey("department"))
				{
					var department = (string) mapFilter["department"];
					filterAll = filterAll.Intersect(from e in ece.Employees
						where e.DepartmentId.Name==department
						select e.Id);
				}
				// фильтрация по критерию "является начальником?"
				if (mapFilter.ContainsKey("isBoss"))
				{
					var isBoss = (bool) mapFilter["isBoss"];
					filterAll = filterAll.Intersect(from e in ece.Employees
						where e.IsBoss==isBoss
						select e.Id);
				}
				// фильтрация по этажу
				if (mapFilter.ContainsKey("floor"))
				{
					var floor = (string) mapFilter["floor"];
					filterAll = filterAll.Intersect(from e in ece.Employees
						where
							SqlFunctions.StringConvert((double) e.Room).Trim().Substring(0, 1)==floor
						select e.Id);
				}

				// отфильтрованный список сотрудников
				return (from e in ece.Employees
					where filterAll.Contains(e.Id)
					select e).ToList();
			}
		}


		/// <summary>
		///     Возвращает отфильтрованный список отделов
		/// </summary>
		/// <param name="mapFilter">Карта фильтров</param>
		private static IEnumerable<Department> GetFilterDepartments(Dictionary<string, object> mapFilter)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				ece.ContextOptions.LazyLoadingEnabled = false;
				var filterAll = from e in ece.Departments
					select e.Id;

				// фильтр по наличию начальника
				if (mapFilter.ContainsKey("boss"))
				{
					var boss = (bool) mapFilter["boss"];
					// список отделов где есть начальник
					var withHeadsOfDepartmentsId = from e in ece.Employees
						where e.IsBoss
						select e.Department;
					// список отделов соответствующих критерию начальника
					filterAll = filterAll.Intersect(from e in ece.Departments
						where withHeadsOfDepartmentsId.Contains(e.Id)==boss
						select e.Id);
				}
				// фильтр по количеству людей
				{
					// список всех отделов - сколько людей в отделе, столько раз повторяется запись
					int[] departmentsEmployees = (from e in ece.Employees
						select (int) e.Department).ToArray();
					// карта отделов (Key: ID отдела | Value: количество сотрудников отдела)
					Dictionary<int, int> mapDepartmentIdToNumberOfEmployees =
						(from e in ece.Departments
							select e.Id).ToDictionary(x => x, x => 0);
					// заполняем карту
					foreach (int x in departmentsEmployees)
					{
						mapDepartmentIdToNumberOfEmployees[x]++;
					}
					// применяем фильтры
					if (mapFilter.ContainsKey("employeesStart"))
					{
						var employeesStart = (int) mapFilter["employeesStart"];
						filterAll = filterAll.Intersect
							(from e in mapDepartmentIdToNumberOfEmployees.Keys
								where mapDepartmentIdToNumberOfEmployees[e]>=employeesStart
								select e);
					}
					if (mapFilter.ContainsKey("employeesEnd"))
					{
						var employeesEnd = (int) mapFilter["employeesEnd"];
						filterAll = filterAll.Intersect
							(from e in mapDepartmentIdToNumberOfEmployees.Keys
								where mapDepartmentIdToNumberOfEmployees[e]<=employeesEnd
								select e);
					}
				}
				return (from e in ece.Departments
					where filterAll.Contains(e.Id)
					select e).ToList();
			}
		}
	}
}