﻿// Created : 2014 02 21 10:17 
// Changed : 2014 03 11 8:33 : Караваев Вадим

using System;
using System.Linq;
using System.Windows;
using ExchangeCardsDAL;


namespace AutoCARD
{
	/// <summary>
	///     Главное окно. Самое главное. Главнее не бывает
	/// </summary>
	public partial class MainWindow
	{
		/// <summary>
		///     Конструктор самого главного окна
		/// </summary>
		public MainWindow()
		{
			InitializeComponent();
			// заполняем все таблички
			UpdateAllGrids(null, null);
		}

		/// <summary>
		///     Обновление всех таблиц
		/// </summary>
		public void UpdateAllGrids(object sender, EventArgs e)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				ece.ContextOptions.LazyLoadingEnabled = false;
				dataGridDepartments.ItemsSource = ece.Departments.ToList();
				dataGridEmployees.ItemsSource = ece.Employees.ToList();
				dataGridCards.ItemsSource = ece.Cards.ToList();
			}
		}

		/// <summary>
		///     Обновление таблицы с карточками
		/// </summary>
		public void UpdateGridCards(object sender, EventArgs e)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				ece.ContextOptions.LazyLoadingEnabled = false;
				dataGridCards.ItemsSource = ece.Cards.ToList();
			}
		}

		/// <summary>
		///     Обновление таблицы с сотрудниками
		/// </summary>
		public void UpdateGridEmployees(object sender, EventArgs e)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				ece.ContextOptions.LazyLoadingEnabled = false;
				dataGridEmployees.ItemsSource = ece.Employees.ToList();
			}
		}

		/// <summary>
		///     Обновление таблицы с отделами
		/// </summary>
		public void UpdateGridDepartments(object sender, EventArgs e)
		{
			using (var ece = new ExchangeCardsEntities())
			{
				ece.ContextOptions.LazyLoadingEnabled = false;
				dataGridDepartments.ItemsSource = ece.Departments.ToList();
			}
		}


		/// <summary>
		///     вызов окна добавления отдела
		/// </summary>
		private void MenuAddDepartment(object sender, RoutedEventArgs e)
		{
			var addDepartment = new AddDepartment(this);
			addDepartment.Closing += UpdateGridDepartments;
			addDepartment.ShowDialog();
		}

		/// <summary>
		///     вызов окна добавления сотрудника
		/// </summary>
		private void MenuAddEmployee(object sender, RoutedEventArgs e)
		{
			var addEmployee = new AddEmployee(this);
			addEmployee.Closing += UpdateGridEmployees;
			addEmployee.ShowDialog();
		}

		/// <summary>
		///     вызов окна добавления карточки
		/// </summary>
		private void MenuAddCard(object sender, RoutedEventArgs e)
		{
			var addEmployee = new AddCard(this);
			addEmployee.Closing += UpdateGridCards;
			addEmployee.ShowDialog();
		}

		/// <summary>
		///     открытие окна фильтров
		/// </summary>
		private void ShowFilters(object sender, RoutedEventArgs e)
		{
			var filters = new Filters(this);
			filters.Closing += EnableMenuItemShowFilters;
			filters.Closing += UpdateAllGrids;
			menuItemShowFilters.IsEnabled = menuItemAdd.IsEnabled = false;
			filters.Show();
		}

		/// <summary>
		///     включение кнопки "фильтры" и "добавить" при закрытии окна фильтров
		/// </summary>
		private void EnableMenuItemShowFilters(object sender, EventArgs e)
		{
			menuItemShowFilters.IsEnabled = menuItemAdd.IsEnabled = true;
		}

		/// <summary>
		///     ну как же не упомнять себя любимого
		/// </summary>
		private void MenuCredits(object sender, RoutedEventArgs e)
		{
			var credits = new Credits();
			credits.ShowDialog();
		}
	}
}