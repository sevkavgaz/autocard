﻿// Created : 2014 02 24 10:26 
// Changed : 2014 03 07 8:58 : Караваев Вадим

using System;


namespace AutoCARD
{
	/// <summary>
	///     Окно содержащее подробный отчет о возникшем, необработанном исключении
	///     Твоя цель, мой юный друг, сделать так что бы оно никогда не появлялось
	/// </summary>
	public partial class ErrorWindow
	{
		public ErrorWindow(Exception ex)
		{
			string errorMsg = "Message: " + ex.Message
				+ "\nSource: " + ex.Source
				+ "\nTargetSite: " + ex.TargetSite
				+ "\nInnerException: " + ex.InnerException
				+ "\nStackTrace" + ex.StackTrace;
			InitializeComponent();
			textBoxError.Text = errorMsg;
		}
	}
}