﻿// Created : 2014 02 21 10:17 
// Changed : 2014 03 07 10:48 : Караваев Вадим

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ExchangeCardsDAL;
using MahApps.Metro.Controls.Dialogs;


namespace AutoCARD
{
	/// <summary>
	///     Окно для добавления нового отдела
	/// </summary>
	public partial class AddDepartment
	{
		// ссылка на главное окно
		private MainWindow _mainWindow;

		/// <summary>
		///     Конструктор окна добавления отдела
		/// </summary>
		/// <param name="mainWindow">ссылка на экземпляр главного окна</param>
		public AddDepartment(MainWindow mainWindow)
		{
			_mainWindow = mainWindow;
			InitializeComponent();

			// устанавливаем ограничения на длину полей
			textBoxDepartmentName.MaxLength = Department.CheckLengthName.Max;
			textBoxDepartmentPhoneNumber.MaxLength = Department.CheckLengthPhone.Max;
			textBoxDepartmentNumberCardsIdentifier.MaxLength = Department.CheckLengthIdentifier.Max;
		}

		/// <summary>
		///     Обработчик события для кнопки "Сохранить"
		/// </summary>
		private void ButtonDepartmentSave_Click(object sender, RoutedEventArgs e)
		{
			if (SaveDepartment())
			{
				Close();
			}
		}

		/// <summary>
		///     Обработчик события для кнопки "Сохранить и добавить"
		/// </summary>
		private void ButtonDepartmentSaveAndAdd_Click(object sender, RoutedEventArgs e)
		{
			if (SaveDepartment())
			{
				Close();
				var addDepartment = new AddDepartment(_mainWindow);
				addDepartment.Closing += _mainWindow.UpdateAllGrids;
				addDepartment.ShowDialog();
			}
		}

		/// <summary>
		///     При нажатии "Enter" в одном из текстовых полей сохраняем отдел и закрываем окно
		/// </summary>
		private void TextBox_EnterConfirm(object sender, KeyEventArgs e)
		{
			if (e.Key==Key.Enter && SaveDepartment())
			{
				Close();
			}
		}

		/// <summary>
		///     При вызове метода добавляется запись, происходит сохранение карточки
		///     в случае успеха возвращаем TRUE
		/// </summary>
		private bool SaveDepartment()
		{
			try
			{
				using (var ece = new ExchangeCardsEntities())
				{
					string wrongMsg = String.Empty;

					// запросы для проверки уникальности
					var checkUnqDepartment = (from e in ece.Departments
						where e.Name==textBoxDepartmentName.Text
						select e).FirstOrDefault();
					var checkUnqPhone = (from e in ece.Departments
						where e.Phone==textBoxDepartmentPhoneNumber.Text
						select e).FirstOrDefault();
					var checkUnqCardsItentifier = (from e in ece.Departments
						where e.CardsIdentifier==textBoxDepartmentNumberCardsIdentifier.Text
						select e).FirstOrDefault();
					// проверка полей на уникальность
					if (checkUnqDepartment!=null)
					{
						this.ShowMessageAsync("Внимание", "Отдел с таким названием уже существует." +
							"\nПопробуйте выбрать другое название.");
						return false;
					}
					if (checkUnqPhone!=null)
					{
						this.ShowMessageAsync("Внимание", "Этот телефон уже принадлежит отделу \""
							+ checkUnqPhone.Name +
							"\".\nВведите другой номер телефона.");
						return false;
					}
					if (checkUnqCardsItentifier!=null)
					{
						this.ShowMessageAsync("Внимание", "Этот идентификатор карт уже принадлежит отделу \""
							+ checkUnqCardsItentifier.Name +
							"\".\nВведите другой идентификатор карт.");
						return false;
					}

					// проверка всех полей на длину
					if (!Department.CheckLength(
						textBoxDepartmentName.Text,
						textBoxDepartmentPhoneNumber.Text,
						textBoxDepartmentNumberCardsIdentifier.Text,
						ref wrongMsg))
					{
						this.ShowMessageAsync("Внимание", wrongMsg);
						return false;
					}

					// добавление отдела и сохранение изменений
					ece.Departments.AddObject(new Department
					{
						Name = textBoxDepartmentName.Text,
						Phone = textBoxDepartmentPhoneNumber.Text,
						CardsIdentifier = textBoxDepartmentNumberCardsIdentifier.Text
					});
					ece.SaveChanges();
					return true;
				}
			}
			catch (Exception ex)
			{
				var errorWindow = new ErrorWindow(ex);
				errorWindow.ShowDialog();
				return false;
			}
		}
	}
}