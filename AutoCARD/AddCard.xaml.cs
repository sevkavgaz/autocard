﻿// Created : 2014 03 03 8:46 
// Changed : 2014 03 07 8:45 : Караваев Вадим

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ExchangeCardsDAL;
using MahApps.Metro.Controls.Dialogs;


namespace AutoCARD
{
	/// <summary>
	///     Окно для добавления новой карты
	/// </summary>
	public partial class AddCard
	{
		// ссылка на экземпляр главного окна
		private readonly MainWindow _mainWindow;

		/// <summary>
		///     Конструктор окна добавления карточки
		/// </summary>
		/// <param name="mainWindow">ссылка на экземпляр главного окна</param>
		public AddCard(MainWindow mainWindow)
		{
			_mainWindow = mainWindow;
			InitializeComponent();

			// заполняем выпадающие списки "Автор Отдел" "Составитель Отдел" "Получатель Отдел"
			using (var ece = new ExchangeCardsEntities())
			{
				var allDepartments = from e in ece.Departments
					where e.Employees.Any()
					select e.Name;

				comboBoxDepartmentAuthor.ItemsSource = allDepartments;
				comboBoxDepartmentDestination.ItemsSource = allDepartments;
				comboBoxDepartmentCompiler.ItemsSource = allDepartments;

				comboBoxDepartmentAuthor.SelectedIndex = 0;
				comboBoxDepartmentDestination.SelectedIndex = 0;
				comboBoxDepartmentCompiler.SelectedIndex = 0;
			}

			// заполняем выпадающий список "Тип записки"
			comboBoxType.ItemsSource = new List<string>
			{
				"Служебная записка",
				"Докладная записка"
			};
			comboBoxType.SelectedIndex = 0;

			// выпадающие списки с сотрудниками заполняем через обработчик события,
			//	я не знаю правильно ли это, но пока что кажется что так удобнее,
			//	чем создавать метод с таким же функционалом. Я буду гореть в аду?
			UpdateComboBoxEmployees(null, null);

			// устанавливаем ограничения на длину полей ввода
			textBoxTheme.MaxLength = Card.CheckLengthTheme.Max;
			textBoxText.MaxLength = Card.CheckLengthText.Max;
		}

		/// <summary>
		///     Обработчик события для кнопки "Сохранить"
		/// </summary>
		private void ButtonEmployeeSave_Click(object sender, RoutedEventArgs e)
		{
			// если сохранили, то закрываем окно
			if (SaveCard())
			{
				Close();
			}
		}

		/// <summary>
		///     Обработчик события для кнопки "Сохранить и добавить"
		/// </summary>
		private void buttonEmployeeSaveAndAdd_Click(object sender, RoutedEventArgs e)
		{
			// если сохранили, закрываем окно и создаем новое
			if (SaveCard())
			{
				Close();
				var addEmployee = new AddEmployee(_mainWindow);
				addEmployee.Closing += _mainWindow.UpdateAllGrids;
				addEmployee.ShowDialog();
			}
		}

		/// <summary>
		///     При нажатии "Enter" в одном из текстовых полей сохраняем карточку и закрываем окно
		/// </summary>
		private void TextBox_EnterConfirm(object sender, KeyEventArgs e)
		{
			// если нажали Enter и как следствие сохранили карточку то закрываем окно
			if (e.Key==Key.Enter && SaveCard())
			{
				Close();
			}
		}

		/// <summary>
		///     При вызове метода добавляется запись, происходит сохранение карточки
		///     в случае успеха возвращаем TRUE
		/// </summary>
		private bool SaveCard()
		{
			try
			{
				using (var ece = new ExchangeCardsEntities())
				{
					string wrongMsg = String.Empty;

					//// проверки полей на длину
					if (!Card.CheckLength(
						textBoxTheme.Text,
						textBoxText.Text,
						ref wrongMsg))
					{
						this.ShowMessageAsync("Внимание", wrongMsg);
						return false;
					}

					// узнаем Id Автора, Составителя, получателя по значению выпадающего списка
					// убрать китайский код! потом, сейчас мне лень.
					var convertAuthorToId = (from e in ece.Employees
						where e.LastName + " " + e.FirstName + " "
							+ e.MiddleName + " [" + e.Post + "]"==comboBoxAuthor.Text
						select e.Id).FirstOrDefault();
					var convertCompilerToId = (from e in ece.Employees
						where e.LastName + " " + e.FirstName + " "
							+ e.MiddleName + " [" + e.Post + "]"==comboBoxCompiler.Text
						select e.Id).FirstOrDefault();
					var convertDestinationToId = (from e in ece.Employees
						where e.LastName + " " + e.FirstName + " "
							+ e.MiddleName + " [" + e.Post + "]"==comboBoxDestination.Text
						select e.Id).FirstOrDefault();

					// проверка не совпадают ли автор, и составитель с получателем
					if (comboBoxAuthor.Text==comboBoxDestination.Text)
					{
						this.ShowMessageAsync("Внимание",
							"Автор и получатель не могут являться одним человеком");
						return false;
					}
					if (comboBoxCompiler.Text==comboBoxDestination.Text)
					{
						this.ShowMessageAsync("Внимание",
							"Составитель и получатель не могут являться одним человеком");
						return false;
					}

					// добавление карточки и сохранение изменений
					ece.Cards.AddObject(new Card
					{
						Date = datePickerDate.DisplayDate,
						Confirmed = (bool) checkBoxIsConfirmed.IsChecked,
						Type = comboBoxType.Text,
						Number = (string) labelNumber.Content,
						Author = convertAuthorToId,
						Compiler = convertCompilerToId,
						Destination = convertDestinationToId,
						Theme = textBoxTheme.Text,
						Text = textBoxText.Text,
						WordFile = (string) labelWordFile.Content
					});
					ece.SaveChanges();
					return true;
				}
			}
				// на случай потопа, пожара
			catch (Exception ex)
			{
				var errorWindow = new ErrorWindow(ex);
				errorWindow.ShowDialog();
				return false;
			}
		}

		/// <summary>
		///     Обновляет список сотрудников, название файла и номер документа в соответствии
		///     с выбранным отделом Очень, очень много плохого кода.
		/// </summary>
		private void UpdateComboBoxEmployees(object sender, EventArgs x)
		{
			// заполняем выпадающий список "Автор" "Составитель" "Получатель"
			using (var ece = new ExchangeCardsEntities())
			{
				// узнаем id выбранных отделов
				var convertDepartmentAuthorToId = (from e in ece.Departments
					where e.Name==comboBoxDepartmentAuthor.Text
					select e.Id).FirstOrDefault();
				var convertDepartmentCompilerToId = (from e in ece.Departments
					where e.Name==comboBoxDepartmentCompiler.Text
					select e.Id).FirstOrDefault();
				var convertDepartmentDestinationToId = (from e in ece.Departments
					where e.Name==comboBoxDepartmentDestination.Text
					select e.Id).FirstOrDefault();

				// заполняем поля сотрудниками из выбранных отделов
				var employeesForDepartmentAuthor = from e in ece.Employees
					where e.Department==convertDepartmentAuthorToId
					select e.LastName + " " + e.FirstName + " " + e.MiddleName + " [" + e.Post + "]";
				var employeesForDepartmentCompiler = from e in ece.Employees
					where e.Department==convertDepartmentCompilerToId
					select e.LastName + " " + e.FirstName + " " + e.MiddleName + " [" + e.Post + "]";
				var employeesForDepartmentDestination = from e in ece.Employees
					where e.Department==convertDepartmentDestinationToId
					select e.LastName + " " + e.FirstName + " " + e.MiddleName + " [" + e.Post + "]";

				// изменяем название файла и номер
				// пускай это будет в скобках, для наглядности
				{
					// префикс
					var convertDepartmentIdToIdentifier = (from e in ece.Departments
						where e.Id==convertDepartmentCompilerToId
						select e.CardsIdentifier).FirstOrDefault();

					// инкрементный постфикс
					// все сотрудники из отдела составителя
					var allEmployeesInDepartmentCompiler = from e in ece.Employees
						where e.Department==convertDepartmentCompilerToId
						select e.Id;
					// номер последнего документа
					var lastDocumentsCompilerDepartments = (from e in ece.Cards
						where allEmployeesInDepartmentCompiler.Contains(e.Compiler)
						orderby e.Number descending
						select e.Number).FirstOrDefault();
					// обновляем название файла и номер документа
					// try и catch весьма извращенский способ в данной ситуации
					// меня нужно наказать
					try
					{
						string[] incrementArr = lastDocumentsCompilerDepartments.Split('/');
						int increment = Convert.ToInt32(incrementArr[1]) + 1;
						labelNumber.Content = convertDepartmentIdToIdentifier + "/" + increment;
						labelWordFile.Content = datePickerDate.SelectedDate.ToString().Split(' ')[0]
							+ "(" + convertDepartmentIdToIdentifier + "-" + increment + ").docx";
					}
					catch
					{
						labelNumber.Content = convertDepartmentIdToIdentifier + "/" + "1";
						labelWordFile.Content = datePickerDate.SelectedDate.ToString().Split(' ')[0]
							+ "(" + convertDepartmentIdToIdentifier + "-" + "1).docx";
					}
				}

				// заполняем выпадающие списки
				comboBoxAuthor.ItemsSource = employeesForDepartmentAuthor;
				comboBoxCompiler.ItemsSource = employeesForDepartmentCompiler;
				comboBoxDestination.ItemsSource = employeesForDepartmentDestination;
				comboBoxAuthor.SelectedIndex = 0;
				comboBoxCompiler.SelectedIndex = 0;
				comboBoxDestination.SelectedIndex = 0;
			}
		}

		/// <summary>
		///     обновить название файла при смене даты
		/// </summary>
		private void datePickerDate_CalendarClosed(object sender, RoutedEventArgs x)
		{
			// это днищеспособ. Исправить!
			UpdateComboBoxEmployees(null, null);
		}
	}
}