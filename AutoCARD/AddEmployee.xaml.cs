﻿// Created : 2014 02 25 9:05 
// Changed : 2014 02 28 10:58 : Караваев Вадим

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ExchangeCardsDAL;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;


namespace AutoCARD
{
	/// <summary>
	///     Окно для добавления нового сотрудника
	/// </summary>
	public partial class AddEmployee
	{
		// ссылка на экземпляр главного окна
		private readonly MainWindow _mainWindow;

		/// <summary>
		///     Конструктор окна добавления еще одного ценного и важного сотрудника
		/// </summary>
		/// <param name="mainWindow">ссылка на экземпляр главного окна</param>
		public AddEmployee(MainWindow mainWindow)
		{
			_mainWindow = mainWindow;
			InitializeComponent();

			// заполняем выпадающий список "Cемейное положение"
			comboBoxRelationShip.ItemsSource = new List<string>
			{
				"Женат/Замужем",
				"Холост/Не замужем",
				"Разведен/Разведена"
			};
			comboBoxRelationShip.SelectedIndex = 0;
			// заполняем выпадающий список "Отдел"
			using (var ece = new ExchangeCardsEntities())
			{
				var allDepartmentsName = from e in ece.Departments
					select e.Name;
				comboBoxDepartment.ItemsSource = allDepartmentsName;
				comboBoxDepartment.SelectedIndex = 0;
			}

			// устанавливаем ограничения на длину полей ввода
			textBoxLastName.MaxLength = Employee.CheckLengthLastName.Max;
			textBoxFirstName.MaxLength = Employee.CheckLengthFirstName.Max;
			textBoxMiddleName.MaxLength = Employee.CheckLengthMiddleName.Max;
			textBoxAddress.MaxLength = Employee.CheckLengthAddress.Max;
			textBoxPhone.MaxLength = Employee.CheckLengthPhone.Max;
			textBoxPost.MaxLength = Employee.CheckLengthPost.Max;
			textBoxPostMsg.MaxLength = Employee.CheckLengthPostMsg.Max;
		}

		/// <summary>
		///     Обработчик события для кнопки "Сохранить"
		/// </summary>
		private void ButtonEmployeeSave_Click(object sender, RoutedEventArgs e)
		{
			if (SaveEmployee())
			{
				Close();
			}
		}

		/// <summary>
		///     Обработчик события для кнопки "Сохранить и добавить"
		/// </summary>
		private void buttonEmployeeSaveAndAdd_Click(object sender, RoutedEventArgs e)
		{
			if (SaveEmployee())
			{
				Close();
				var addEmployee = new AddEmployee(_mainWindow);
				addEmployee.Closing += _mainWindow.UpdateAllGrids;
				addEmployee.ShowDialog();
			}
		}

		/// <summary>
		///     При нажатии "Enter" в одном из текстовых полей сохраняем карточку и закрываем окно
		/// </summary>
		private void TextBox_EnterConfirm(object sender, KeyEventArgs e)
		{
			if (e.Key==Key.Enter && SaveEmployee())
			{
				Close();
			}
		}

		/// <summary>
		///     При вызове метода добавляется запись, происходит сохранение карточки
		///     в случае успеха возвращаем TRUE
		/// </summary>
		private bool SaveEmployee()
		{
			try
			{
				using (var ece = new ExchangeCardsEntities())
				{
					string wrongMsg = String.Empty;

					// проверка на уникальность номера телефона
					var checkUnqPhone = (from e in ece.Employees
						where e.Phone==textBoxPhone.Text
						select e).FirstOrDefault();
					if (checkUnqPhone!=null)
					{
						this.ShowMessageAsync("Внимание", "Этот телефон уже закреплен за сотрудником \""
							+ checkUnqPhone.LastName + " " + checkUnqPhone.FirstName
							+ " " + checkUnqPhone.MiddleName + "\".\nВведите другой номер телефона.");
						return false;
					}

					// проверки полей на длину
					if (!Employee.CheckLength(
						textBoxLastName.Text,
						textBoxFirstName.Text,
						textBoxMiddleName.Text,
						textBoxAddress.Text,
						textBoxPhone.Text,
						textBoxPost.Text,
						textBoxPostMsg.Text,
						ref wrongMsg))
					{
						this.ShowMessageAsync("Внимание", wrongMsg);
						return false;
					}

					// узнаем Id отдела по значению выпадающего списка
					// это нужно при создании записи, ага
					var convertNameDepartmentToId = (from e in ece.Departments
						where e.Name==comboBoxDepartment.Text
						select e.Id).FirstOrDefault();

					// добавление сотрудника и сохранение изменений
					ece.Employees.AddObject(new Employee
					{
						LastName = textBoxLastName.Text,
						FirstName = textBoxFirstName.Text,
						MiddleName = textBoxMiddleName.Text,
						Address = textBoxAddress.Text,
						Phone = textBoxPhone.Text,
						Relationship = comboBoxRelationShip.Text,
						Department = Convert.ToInt32(convertNameDepartmentToId),
						IsBoss = (bool) checkBoxIsBoss.IsChecked,
						Post = textBoxPost.Text,
						PostMsg = textBoxPostMsg.Text,
						Salary = Convert.ToInt32(numericSalary.Value),
						Room = Convert.ToInt32(numericRoom.Value),
						Standing = Convert.ToInt32(numericStanding.Value),
					});
					ece.SaveChanges();
					return true;
				}
			}
			catch (Exception ex)
			{
				var errorWindow = new ErrorWindow(ex);
				errorWindow.ShowDialog();
				return false;
			}
		}

		// при фокусировании на числовом поле выделить все содержимое
		private void NumericGotFocus(object sender, RoutedEventArgs e)
		{
			var a = (NumericUpDown) sender;
			a.SelectAll();
		}
	}
}