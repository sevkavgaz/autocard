﻿#pragma checksum "..\..\..\Filters.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "CC230FA916DF3531B9BC1E066D2216CF"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.296
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using MahApps.Metro.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AutoCARD {
    
    
    /// <summary>
    /// Filters
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class Filters : MahApps.Metro.Controls.MetroWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 9 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label3;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label4;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label5;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label6;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label7;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label8;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label9;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label10;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label11;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label12;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label13;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label14;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label15;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button resetFiltersCards;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button resetFiltersEmployees;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button resetFiltersDepartments;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button apply;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateStart;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateEnd;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxStatus;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxType;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxDepartmentAuthor;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxAuthor;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxDepartmentCompiler;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxDepartmentDestination;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxCompiler;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxDestination;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxDepartment;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxIsBoss;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxFloor;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxBoss;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label16;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label17;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label18;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.NumericUpDown employeesStart;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\Filters.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.NumericUpDown employeesEnd;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AutoCARD;component/filters.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Filters.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.label3 = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.label4 = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.label5 = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.label6 = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.label7 = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.label8 = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.label9 = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.label10 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.label11 = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.label12 = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.label13 = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.label14 = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.label15 = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.resetFiltersCards = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\..\Filters.xaml"
            this.resetFiltersCards.Click += new System.Windows.RoutedEventHandler(this.resetFiltersCards_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.resetFiltersEmployees = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\..\Filters.xaml"
            this.resetFiltersEmployees.Click += new System.Windows.RoutedEventHandler(this.resetFiltersEmployees_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.resetFiltersDepartments = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\..\Filters.xaml"
            this.resetFiltersDepartments.Click += new System.Windows.RoutedEventHandler(this.resetFiltersDepartments_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.apply = ((System.Windows.Controls.Button)(target));
            
            #line 26 "..\..\..\Filters.xaml"
            this.apply.Click += new System.Windows.RoutedEventHandler(this.apply_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.dateStart = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 21:
            this.dateEnd = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 22:
            this.comboBoxStatus = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 23:
            this.comboBoxType = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 24:
            this.comboBoxDepartmentAuthor = ((System.Windows.Controls.ComboBox)(target));
            
            #line 31 "..\..\..\Filters.xaml"
            this.comboBoxDepartmentAuthor.DropDownClosed += new System.EventHandler(this.UpdateComboBoxEmployees);
            
            #line default
            #line hidden
            return;
            case 25:
            this.comboBoxAuthor = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 26:
            this.comboBoxDepartmentCompiler = ((System.Windows.Controls.ComboBox)(target));
            
            #line 33 "..\..\..\Filters.xaml"
            this.comboBoxDepartmentCompiler.DropDownClosed += new System.EventHandler(this.UpdateComboBoxEmployees);
            
            #line default
            #line hidden
            return;
            case 27:
            this.comboBoxDepartmentDestination = ((System.Windows.Controls.ComboBox)(target));
            
            #line 34 "..\..\..\Filters.xaml"
            this.comboBoxDepartmentDestination.DropDownClosed += new System.EventHandler(this.UpdateComboBoxEmployees);
            
            #line default
            #line hidden
            return;
            case 28:
            this.comboBoxCompiler = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 29:
            this.comboBoxDestination = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 30:
            this.comboBoxDepartment = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 31:
            this.comboBoxIsBoss = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 32:
            this.comboBoxFloor = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 33:
            this.comboBoxBoss = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 34:
            this.label16 = ((System.Windows.Controls.Label)(target));
            return;
            case 35:
            this.label17 = ((System.Windows.Controls.Label)(target));
            return;
            case 36:
            this.label18 = ((System.Windows.Controls.Label)(target));
            return;
            case 37:
            this.employeesStart = ((MahApps.Metro.Controls.NumericUpDown)(target));
            return;
            case 38:
            this.employeesEnd = ((MahApps.Metro.Controls.NumericUpDown)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

